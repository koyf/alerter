from setuptools import setup

setup(name='alerter',
      version='0.0.1',
      description='Telegram alerter for market.csgo.com',
      author='Koyf Williams',
      author_email='nevigor16@gmail.com',
      packages=['alerter'],
      install_requires=['telebot'])
