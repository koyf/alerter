import logging
import requests


def get_trades(key):
    """
    Берет список трейдов из маркета по переданому ключу, и возвращает имя предмета, если он был продан,
    или ничего, если пердмет не продан.
    :param key: (str) секретный ключ пользователя
    :return: (str/None) возвращает имя проданого предмета, или же None, если ничего не продано
    """
    logger = logging.getLogger('alerter.market.get_trades')

    if type(key) is not str:
        raise TypeError

    link = f'https://market.dota2.net/api/v2/items?key={key}'

    try:
        r = requests.get(link)
        r_json = r.json()
        items = r_json['items'] or []
        logger.info(key)
        logger.info(str(r_json).encode('ascii', 'ignore'))

        for item in items:
            if item['status'] == '2':
                return item['market_hash_name']

    except Exception as err:
        logger.error(err)


def ping(key):
    """
    Запрос на маркет для поддержки пользователя онлайн
    :param key: (str) секретный ключ пользователя
    :return: (requests.Response) возвращает результат запроса
    """
    logger = logging.getLogger('alerter.market.ping')

    if type(key) is not str:
        raise TypeError

    link = f'https://market.csgo.com/api/v2/ping?key={key}'

    try:
        r = requests.get(link)
        r = r.json()
        logger.info(key)
        logger.info(str(r).encode('ascii', 'ignore'))
        return r
    except Exception as err:
        logger.error(err)
