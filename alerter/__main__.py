import sys
import logging
import time
import json
import threading

from alerter.alerter import telegram
from alerter.alerter import market


def checker(name, key, token, chat_id):
    """
    Проверяет был ли продан предмет, если да, отправляет сообщение в телеграм с именем предмета и именем пользователя,
    у которого предмет был продан
    :param name: (str) имя пользователя
    :param key: (str) секретный ключ пользователя
    :param token: (str) секретный токен бота
    :param chat_id: (str) идентификатор чата, в который бот отправит сообщение
    :return: None
    """
    while True:
        mess = market.get_trades(key)
        market.ping(key)

        if mess:
            telegram.send_message(token, chat_id, f'{name} - {mess}')

        time.sleep(30)


def main():
    logger = logging.getLogger('alerter')
    logger.setLevel(logging.WARN)

    fh = logging.FileHandler('logs/alerter.log')
    formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
    fh.setFormatter(formatter)

    logger.addHandler(fh)
    logger.info('Start')

    argv = sys.argv

    if len(argv) < 2:
        raise TypeError('Enter path to json file')

    file = open(argv[1])
    data = file.read()
    users = json.loads(data)

    for user in users:
        t = threading.Thread(target=checker, args=(user['name'], user['key'], user['token'], user['chatId']))
        t.start()


if __name__ == '__main__':
    main()
