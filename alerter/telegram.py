import logging

from telebot import TeleBot


def send_message(token, chat_id, message):
    """
    Отправляет сообщение в telegram от имени бота, по указаному токену, в указаный чат, переданое сообщение
    :param token: (str) секретный токен бота
    :param chat_id: (str) идентификатор чата с пользователем. Пользователь должен первым написать сообщение боту
    :param message: (str) сообщение которое бот отправит в указаный чат
    :return: None
    """
    bot = TeleBot(token)
    logger = logging.getLogger('alerter.telegram.send_message')

    try:
        bot.send_message(chat_id, message)
        logger.info((token, chat_id))
        logger.info(message.encode('ascii', 'ignore'))
    except Exception as err:
        logger.error(err)
